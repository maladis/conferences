package com.my.DBConnection;

import com.my.listeners.AppContextListener;
import org.apache.commons.dbcp.BasicDataSource;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class DBCPSetProperties {
    private static final org.slf4j.Logger logger= LoggerFactory.getLogger(DBCPSetProperties.class);

    public static void setProperties(){
        BasicDataSource ds=DBCPDataSource.getDataSource();
        try (InputStream input = new FileInputStream("C:\\Users\\admin\\Desktop\\EPAM\\project\\conferences\\untitled\\src\\main\\resources\\db.properties")) {
            Properties prop = new Properties();
            prop.load(input);
            ds.setMinIdle(Integer.parseInt(prop.getProperty("DbMySql.MinIdle")));
            ds.setMaxIdle(Integer.parseInt(prop.getProperty("DbMySql.MaxIdle")));
            ds.setMaxOpenPreparedStatements(Integer.parseInt(prop.getProperty("DbMySql.MaxOpenPreparedStatements")));
            ds.setUrl(prop.getProperty("DbMySql.Url"));
            ds.setConnectionProperties(prop.getProperty("DbMySql.ConnectionProperties"));
            ds.setUsername(prop.getProperty("DbMySql.Username"));
            ds.setPassword(prop.getProperty("DbMySql.Password"));
            ds.setDriverClassName(prop.getProperty("DbMySql.DriverClassName"));
        } catch (FileNotFoundException e) {
            logger.error(e.getMessage(),e);
        } catch (IOException e) {
            logger.error(e.getMessage(),e);
        }
    };
}
